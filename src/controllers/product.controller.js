const db = require('../models');
const Product = db.products;
const Op = db.Sequelize.Op;

// Create and Save a new Product -> Crear y guardar un producto nuevo
exports.create = (req, res) => {
  // Validate request -> Validar solicitud
  if (!req.body.title) {
    res.status(400).send({
      message: 'El contenido no puede estar vacío!',
    });
    return;
  }

  // Create a Product -> Crear un producto
  const product = {
    title: req.body.title,
    description: req.body.description,
    published: req.body.published ? req.body.published : false,
  };

  // Save Product in the database -> Guardar producto en la base de datos
  Product.create(product)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || 'Se produjo un error al crear el producto.',
      });
    });
};

// Retrieve all Products from the database. -> Recuperar todos los productos de la base de datos.
exports.findAll = (req, res) => {
  const title = req.query.title;
  var condition = title ? { title: { [Op.like]: `%${title}%` } } : null;

  Product.findAll({ where: condition })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || 'Se produjo un error al recuperar productos.',
      });
    });
};

// Encuentra un solo producto con una identificación -> Encuentra un solo producto con una identificación
exports.findOne = (req, res) => {
  const id = req.params.id;

  Product.findByPk(id)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: 'Error al recuperar el producto con id=' + id,
      });
    });
};

// Actualizar un producto por el id en la solicitud -> Actualizar un producto por el id en la solicitud
exports.update = (req, res) => {
  const id = req.params.id;

  Product.update(req.body, {
    where: { id: id },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: 'El producto se actualizó correctamente.',
        });
      } else {
        res.send({
          message: `No se puede actualizar el producto con id=${id}. Tal vez el producto no se encontró o el cuerpo requerido está vacío.!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: 'Error al actualizar el producto con id=' + id,
      });
    });
};

//Eliminar un producto con la identificación especificada en la solicitud
exports.delete = (req, res) => {
  const id = req.params.id;

  Product.destroy({
    where: { id: id },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: 'El producto se eliminó correctamente!',
        });
      } else {
        res.send({
          message: `No se puede eliminar el producto con id=${id}. Quizás no se encontró el producto!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: 'No se pudo borrar el Producto con id=' + id,
      });
    });
};

// Elimina todos los productos de la base de datos. -> Elimina todos los productos de la base de datos.
exports.deleteAll = (req, res) => {
  Product.destroy({
    where: {},
    truncate: false,
  })
    .then((nums) => {
      res.send({
        message: `${nums} Los productos se eliminaron correctamente!`,
      });
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || 'Se produjo un error al eliminar todos los productos.',
      });
    });
};

// buscar todos los productos publicados -> buscar todos los productos publicados
exports.findAllPublished = (req, res) => {
  Product.findAll({ where: { published: true } })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || 'Se produjo un error al recuperar productos.',
      });
    });
};
