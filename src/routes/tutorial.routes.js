module.exports = (app) => {
  const products = require('../controllers/product.controller.js');

  var router = require('express').Router();

  // Create a new Product - Crea un nuevo producto
  router.post('/', products.create);

  // Retrieve all Products - Recuperar todos los productos
  router.get('/', products.findAll);

  // Retrieve all published Products - Recuperar todos los productos publicados
  router.get('/published', products.findAllPublished);

  // Retrieve a single Product with id - Recuperar un solo producto con id
  router.get('/:id', products.findOne);

  // Update a Product with id - Actualizar un producto con id
  router.put('/:id', products.update);

  // Delete a Product with id - Eliminar un producto con id
  router.delete('/:id', products.delete);

  // Delete all Products - Eliminar todos los productos
  router.delete('/', products.deleteAll);

  app.use('/api/products', router);
};
